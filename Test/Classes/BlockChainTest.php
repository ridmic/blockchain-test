<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 07/07/18
 * Time: 12:05
 */

namespace Ridmic\Test;

use Ridmic\Classes\Chain;
use Ridmic\Classes\NullChainItem;
use Ridmic\Classes\Block;
use Ridmic\Classes\BlockChain;
use PHPUnit\Framework\TestCase;

class BlockChainTest extends TestCase
{
    /** @var  Chain */
    protected $chain;

    /** @var  BlockChain */
    protected $blockChain;

    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        $this->chain = new Chain();
        $this->blockChain = new BlockChain( $this->chain );
    }

    public function testConstruction() {
        $this->assertInstanceOf( BlockChain::class, $this->blockChain);
    }

    public function testAccessors() {
        $this->assertEquals( 2, $this->blockChain->getDifficulty(), "Difficulty does not match" );
        $this->assertTrue( $this->chain === $this->blockChain->getChain(), "Difficulty does not match" );
    }

    public function testInitialisation() {
        $this->assertTrue( $this->blockChain->initialiseChain(), "Unable to initialise");

        // We should have our single genesis block
        $this->assertTrue( $this->chain->length() === 1 );
        /**
         * @var Block $genesis
         */
        $genesis = $this->chain->first();
        $this->assertInstanceOf( Block::class, $genesis);
        $this->assertTrue( $genesis->isValid());
        $this->assertEquals( 0, $genesis->getNonce());
        $this->assertEquals( BlockChain::$genesisKey, $genesis->getContent());
        $this->assertEquals( "0", $genesis->getPreviousChecksum());
    }

    public function testMiningABlock() {
        $this->assertTrue( $this->blockChain->initialiseChain(), "Unable to initialise");

        /**
         * @var Block $block
         */
        $block = $this->blockChain->mineBlock( "some data" );
        $this->assertInstanceOf( Block::class, $block);
        $this->assertTrue( $block->isValid());
        $this->assertEquals( "some data", $block->getContent());
        $this->assertEquals( $this->chain->first()->getChecksum(), $block->getPreviousChecksum());
    }

    public function testMineBlockBoundaries() {
        $this->assertTrue( $this->blockChain->initialiseChain(), "Unable to initialise");

        // An empty block
        $data = "";
        $block = $this->blockChain->mineBlock( $data );
        $this->assertInstanceOf( NullChainItem::class, $block);

        // An single byte block
        $data = "A";
        $block = $this->blockChain->mineBlock( $data );
        $this->assertInstanceOf( Block::class, $block);

        // An max size  block
        $data = str_pad("", $this->blockChain->getMaxDataSize(), "A", STR_PAD_LEFT);
        $block = $this->blockChain->mineBlock( $data );
        $this->assertInstanceOf( Block::class, $block);

        // A 'too big' block
        $data = str_pad("", $this->blockChain->getMaxDataSize()+1, "A", STR_PAD_LEFT);
        $block = $this->blockChain->mineBlock( $data );
        $this->assertInstanceOf( NullChainItem::class, $block);
    }


    public function testProofOfWork() {
        $this->assertTrue( $this->blockChain->initialiseChain(), "Unable to initialise");

        $block = new Block("some data", "previous checksum");

        $proof = $this->blockChain->proofOfWork( $block );
        $this->assertTrue( $this->blockChain->isValidProof( $block, $proof ));
    }

    public function testValidate() {
        $this->assertTrue( $this->blockChain->initialiseChain(), "Unable to initialise");

        // Mine some blocks

        /**
         * @var Block $block
         */
        $block = $this->blockChain->mineBlock( "some data1" );
        $this->assertInstanceOf( Block::class, $block);
        $this->assertTrue( $block->isValid());

        $block = $this->blockChain->mineBlock( "some data2" );
        $this->assertInstanceOf( Block::class, $block);
        $this->assertTrue( $block->isValid());

        $block = $this->blockChain->mineBlock( "some data3" );
        $this->assertInstanceOf( Block::class, $block);
        $this->assertTrue( $block->isValid());

        // Should have 4 items (inc Genesis)
        $this->assertEquals( 4, $this->chain->length());
        // Should be valid
        $this->assertTrue( $this->blockChain->validate());

        // Should have all of the expected items
        $this->assertTrue( $this->chain->get( 0 )->getContent() === BlockChain::$genesisKey , "Failed to get item0" );
        $this->assertTrue( $this->chain->get( 1 )->getContent() === "some data1" , "Failed to get item1" );
        $this->assertTrue( $this->chain->get( 2 )->getContent() === "some data2" , "Failed to get item2" );
        $this->assertTrue( $this->chain->get( 3 )->getContent() === "some data3" , "Failed to get item3" );
    }
}
