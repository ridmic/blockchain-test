<?php

namespace Ridmic\Test;

use Ridmic\Classes\Chain;
use Ridmic\Classes\NullChainItem;
use PHPUnit\Framework\TestCase;

class ChainTest extends TestCase
{
    /** @var  Chain */
    protected $chain;

    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        $this->chain = new Chain();
    }

    public function testConstruction() {
        $this->assertInstanceOf( Chain::class, $this->chain);
    }

    public function testAccessors() {
        $this->assertEquals(  0, $this->chain->key(), "Position (key) not correct" );
        $this->assertEquals( -1, $this->chain->maxLength(), "Content does not match" );
    }

    public function testDefaultInit() {
        $this->assertTrue( $this->chain->init());
        $this->assertEquals(  0, $this->chain->key(), "Position (key) not correct" );
        $this->assertEquals(  0, $this->chain->length(), "Length not correct" );
        $this->assertEquals(  -1, $this->chain->maxLength(), "MaxLength not correct" );
    }

    public function testSizedInit() {
        $this->assertTrue( $this->chain->init( 10 ));
        $this->assertEquals(  0, $this->chain->key(), "Position (key) not correct" );
        $this->assertEquals(  0, $this->chain->length(), "Length not correct" );
        $this->assertEquals(  10, $this->chain->maxLength(), "MaxLength not correct" );
        $this->assertNotEquals(  1, $this->chain->maxLength(), "MaxLength not correct" );
    }

    public function testAppendingSingleItem() {

        $item = new NullChainItem();

        $this->assertTrue( $this->chain->append( $item ), "Failed to add item" );

        $this->assertFalse( $this->chain->isEmpty(), "List should not be empty");
        $this->assertTrue( $this->chain->length() === 1, "List should have 1 item(s)" );
    }

    public function testInitAfterAppendingSingleItem() {

        $item = new NullChainItem();

        $this->assertTrue( $this->chain->append( $item ), "Failed to add item" );

        $this->assertFalse( $this->chain->isEmpty(), "List should not be empty");
        $this->assertTrue( $this->chain->length() === 1, "List should have 1 item(s)" );

        // Init and recheck
        $this->assertTrue( $this->chain->init());
        $this->assertEquals(  0, $this->chain->key(), "Position (key) not correct" );
        $this->assertEquals(  0, $this->chain->length(), "Length not correct" );
        $this->assertEquals(  -1, $this->chain->maxLength(), "MaxLength not correct" );
    }

    public function testAppendingMultipleItems() {

        $item = new NullChainItem();

        $this->assertTrue( $this->chain->append( $item ), "Failed to add item" );
        $this->assertTrue( $this->chain->append( $item ), "Failed to add item" );
        $this->assertTrue( $this->chain->append( $item ), "Failed to add item" );

        $this->assertFalse( $this->chain->isEmpty(), "List should not be empty");
        $this->assertTrue( $this->chain->length() === 3, "List should have 1 item(s)" );
    }

    public function testAppendingTooManyTransactions() {

        $item = new NullChainItem();

        $this->assertTrue( $this->chain->init( 10 ));
        for ( $i = 0 ; $i < $this->chain->maxLength() ; $i++ )
            $this->chain->append( $item );
        $this->assertFalse( $this->chain->append( $item ), "Failed: shouldn't have added this" );

        $this->assertFalse( $this->chain->isEmpty(), "List should not be empty");
        $this->assertTrue( $this->chain->length() === $this->chain->maxLength(), "List should have MAX item(s)" );
    }

    public function testGetAccessor() {
        // Add some items
        $item1 = new NullChainItem();
        $item2 = new NullChainItem();
        $item3 = new NullChainItem();

        $this->assertTrue( $this->chain->append( $item1 ), "Failed to add item1" );
        $this->assertTrue( $this->chain->append( $item2 ), "Failed to add item2" );
        $this->assertTrue( $this->chain->append( $item3 ), "Failed to add item3" );

        // and now test our the accessor (in-bounds)
        $this->assertTrue( $this->chain->get( 0 ) === $item1 , "Failed to get item1" );
        $this->assertTrue( $this->chain->get( 1 ) === $item2 , "Failed to get item2" );
        $this->assertTrue( $this->chain->get( 2 ) === $item3 , "Failed to get item3" );

        // and now test our the accessor (out-of-bounds)

        // This should return a different (new) NullChainItem
        $newItem = $this->chain->get( 3 );
        $this->assertTrue( $newItem !== $item1 , "Failed to get new item" );
        $this->assertTrue( $newItem !== $item2 , "Failed to get new item" );
        $this->assertTrue( $newItem !== $item3 , "Failed to get new item" );
        $this->assertInstanceOf( NullChainItem::class, $newItem );

        $newItem = $this->chain->get( -1 );
        $this->assertTrue( $newItem !== $item1 , "Failed to get new item" );
        $this->assertTrue( $newItem !== $item2 , "Failed to get new item" );
        $this->assertTrue( $newItem !== $item3 , "Failed to get new item" );
        $this->assertInstanceOf( NullChainItem::class, $newItem );
    }

    public function testFirstAccessor() {
        // Add some items
        $item1 = new NullChainItem();
        $item2 = new NullChainItem();
        $item3 = new NullChainItem();

        $this->assertTrue( $this->chain->append( $item1 ), "Failed to add item1" );
        $this->assertTrue( $this->chain->append( $item2 ), "Failed to add item2" );
        $this->assertTrue( $this->chain->append( $item3 ), "Failed to add item3" );

        // and now test our the accessor (in-bounds)
        $this->assertTrue( $this->chain->first() === $item1 , "Failed to get item" );
    }

    public function testLastAccessor() {
        // Add some items
        $item1 = new NullChainItem();
        $item2 = new NullChainItem();
        $item3 = new NullChainItem();

        $this->assertTrue( $this->chain->append( $item1 ), "Failed to add item1" );
        $this->assertTrue( $this->chain->append( $item2 ), "Failed to add item2" );
        $this->assertTrue( $this->chain->append( $item3 ), "Failed to add item3" );

        // and now test our the accessor (in-bounds)
        $this->assertTrue( $this->chain->last() === $item3 , "Failed to get item" );
    }

    public function testManualIteration() {
        // Add some items
        $item1 = new NullChainItem();
        $item2 = new NullChainItem();
        $item3 = new NullChainItem();

        $this->assertTrue( $this->chain->append( $item1 ), "Failed to add item1" );
        $this->assertTrue( $this->chain->append( $item2 ), "Failed to add item2" );
        $this->assertTrue( $this->chain->append( $item3 ), "Failed to add item3" );

        $check = [$item1, $item2, $item3];
        $this->chain->rewind();
        $this->assertTrue( $this->chain->valid(), "Failed to validate" );
        while ($this->chain->valid())
        {
            $key = $this->chain->key();
            $value = $this->chain->current();

            $this->assertTrue( $value === $check[$key] , "Failed to add item" );

            $this->chain->next();
        }
        // Should be at position 3 (end of list)
        $this->assertEquals(  3, $this->chain->key(), "Position (key) not correct" );
    }

    public function testForEachIteration() {
        // Add some items
        $item1 = new NullChainItem();
        $item2 = new NullChainItem();
        $item3 = new NullChainItem();

        $this->assertTrue( $this->chain->append( $item1 ), "Failed to add item1" );
        $this->assertTrue( $this->chain->append( $item2 ), "Failed to add item2" );
        $this->assertTrue( $this->chain->append( $item3 ), "Failed to add item3" );

        $check = [$item1, $item2, $item3];

        // For each....
        $this->chain->rewind();
        $this->assertTrue( $this->chain->valid(), "Failed to validate" );
        $index = 0;
        foreach ($this->chain as $chainItem )
        {
            $this->assertTrue( $chainItem === $check[$index] , "Failed to add item" );
            $index++;
        }
        // Should be at position 3 (end of list)
        $this->assertEquals(  3, $this->chain->key(), "Position (key) not correct" );
    }

}
