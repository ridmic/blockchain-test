<?php

namespace Ridmic\Test;

use Ridmic\Classes\ChainItem;
use Ridmic\Classes\NullChainItem;
use PHPUnit\Framework\TestCase;

class NullChainItemTest extends TestCase
{
    /** @var  NullChainItem */
    protected $chainItem;

    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        $this->chainItem= new NullChainItem();
    }

    public function testConstruction() {
        $this->assertInstanceOf( ChainItem::class, $this->chainItem);
    }

    public function testAccessors() {
        $this->assertGreaterThan( 0, $this->chainItem->getTimestamp(), "Timestamp does not appear valid" );
        $this->assertLessThanOrEqual( time(), $this->chainItem->getTimestamp(), "Timestamp does not appear valid" );
        $this->assertEquals( $this->chainItem->calculateChecksum(), $this->chainItem->getChecksum(), "Checksum does not match" );
    }

    public function testChecksums() {
        $this->assertEquals( $this->chainItem->calculateChecksum(), $this->chainItem->getChecksum(), "Checksum does not match" );
        $this->assertEquals( $this->chainItem->calculateChecksum(), NullChainItem::$nullChecksum, "Checksum does not match" );
    }

    public function testSettingChecksum() {
        $this->chainItem->setChecksum('ABCDEF');
        $this->assertEquals( 'ABCDEF', $this->chainItem->getChecksum(), "Checksum does not match" );
    }

    public function testValidate() {
        $this->assertFalse( $this->chainItem->isValid(), "The item should always be invalid" );
    }

}
