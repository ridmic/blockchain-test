<?php


namespace Ridmic\Test;

use Ridmic\Classes\Transaction;
use PHPUnit\Framework\TestCase;

class TransactionTest extends TestCase
{
    /** @var  Transaction */
    protected $trans;
    /** @var string */
    protected $owner;
    /** @var string */
    protected $content;

    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        $this->owner = "Mike";
        $this->content = "Some Content";
        $this->trans = new Transaction( $this->owner, $this->content );
    }

    public function testConstruction() {
        $this->assertInstanceOf( Transaction::class, $this->trans);
    }

    public function testAccessors() {
        $this->assertEquals(  $this->owner, $this->trans->getOwner(), "Owner does not match" );
        $this->assertEquals( $this->content, $this->trans->getContent(), "Content does not match" );
        $this->assertGreaterThan( 0, $this->trans->getTimestamp(), "Timestamp does not appear valid" );
        $this->assertLessThanOrEqual( time(), $this->trans->getTimestamp(), "Timestamp does not appear valid" );
        $this->assertEquals( $this->trans->calculateChecksum(), $this->trans->getChecksum(), "Checksum does not match" );
    }

    public function testChecksums() {
        $trans = new Transaction( "A Nother", "Other Content" );
        $this->assertEquals( $this->trans->calculateChecksum(), $this->trans->getChecksum(), "Checksum does not match" );
        $this->assertNotEquals( $this->trans->calculateChecksum(), $trans->calculateChecksum(), "Checksum should not match" );
        $this->assertNotEquals( $this->trans->getChecksum(), $trans->getChecksum(), "Checksum should not match" );
    }

    public function testValidate() {
        $this->assertTrue( $this->trans->isValid(), "THe transaction is not valid" );
    }
}
