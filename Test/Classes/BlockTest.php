<?php

namespace Ridmic\Test;

use Ridmic\Classes\Block;
use PHPUnit\Framework\TestCase;

class BlockTest extends TestCase
{
    /** @var  Block */
    protected $block;

    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        $this->block = new Block("some data", "previous checksum");
    }

    public function testConstruction() {
        $this->assertInstanceOf( Block::class, $this->block);
    }

    public function testAccessors() {
        $this->assertEquals(  0, $this->block->getNonce(), "Nonce does not match" );
        $this->assertEquals( $this->block->calculateChecksum(), $this->block->getChecksum(), "Checksum does not match" );
        $this->assertEquals( "previous checksum", $this->block->getPreviousChecksum(), "Prev Checksum does not match" );
        $this->assertEquals( "some data", $this->block->getContent(), "Datadoes not match" );

        $this->assertGreaterThan( 0, $this->block->getTimestamp(), "Timestamp does not appear valid" );
        $this->assertLessThanOrEqual( time(), $this->block->getTimestamp(), "Timestamp does not appear valid" );
    }

    public function testChecksums() {
        $block = new Block( "some data2", "previous checksum2" );
        $this->assertEquals( $this->block->calculateChecksum(), $this->block->getChecksum(), "Checksum does not match" );
        $this->assertNotEquals( $this->block->calculateChecksum(), $block->calculateChecksum(), "Checksum should not match" );
        $this->assertNotEquals( $this->block->getChecksum(), $block->getChecksum(), "Checksum should not match" );
    }

    public function testValidate() {
        $this->assertTrue( $this->block->isValid(), "THe transaction is not valid" );
    }

    public function testSettingNonce() {
        $this->assertEquals(  0, $this->block->getNonce(), "Nonce does not match" );
        $this->block->setNonce(101);
        $this->assertEquals(  101, $this->block->getNonce(), "Nonce does not match" );
    }
}
