# Blockchain Example

This is a sample project to learn and test out the functionality and internals of a Blockchain

The main Blockchain classes sit under the Src/Classes folder. Appropriate Unittests sit under Test/Classes.

There are three main entry points to test out the code:

1. Command Line

2. HTML Pages

3. Simple RESTful API

## Command Line
To run from the command line, you can just run any of the following directly (if you have a php intepreter):

**SimpleBlockChainExample.php**:  This example adds some simple string based entities to the Blockchain and then validates
the chain when done. This code also uses the simplified persistence of the Blockchain class to persist the chain between
invocations. As I am only testing out the Blockchain parts, the persistence is left as this simple file.
 

**TransactionalBlockChainExample.php**:  This example creates individual Transactions entities and the adds these as 
individual Blocks to the Blockchain. The Blockchain is then validated for correctness.

**MultipleTransactionalBlockChainExample.php**:  This example creates a set of Transactions entities and the adds these as a
single Block to the Blockchain. The Blockchain is then validated for correctness.

## HTML Pages
The HTML pages give a simple level of interaction to test out the Blockchain. You can start the process listening locally
by opening up a termainal, moving to the Src folder and then using the command: 

**php -S localhost:8080 SimpleBlockchainService.php**. 


Changes can be made to the code whilst this process is running and they will be reflected in the next invocation.

There are a few routes active for the HTML:

1. localhost:8080/ -> The home/welcome page. 
2. localhost:8080/chain -> Shows the current state of the Blockchain and allows you to add blocks
3. localhost:8080/chain/:item -> Shows details of the Nth item in the Blockchain. You can also get here by clicking on the
**more** link in the summary table.
 
Note: Adding new blocks utilises the RESTful API interface to add the block. Successful adds will return to the summary
page, fails will show the json error from the API call. You will need to press the Browser Back button to return to the
summary page.


## Simple API
The API interface is a simple RESTFul API. It only currently supports GET/POST as Blockchains are immutable once a block 
has been added - i.e. there is no need for PUT/PATCH/DELETE against this interface. The API is serviced through the same
code as the HTML Pages, so you run the module in the same manner (see above).

You can use the [Postman](https://www.getpostman.com/) application to drive the API.

There are a few active routes for the API:

1. GET /api/chain -> Returns the current list of items on the Blockchain as JSON.
2. POST /api/chain (form: content = 'text to add') -> inserts a new item on the Blockchain

THe API POST is used by the HTML Pages to add items.


## Cloning
This application uses minimal dependencies. Composer is used for autoloading, setting up the test folders and pulling
in PHPUnitTest. The routing is done via the ['Dispatch.php'](https://github.com/noodlehaus/dispatch) module - which is 
ideal for simple PoCs.   

Feel free to use this at your will, there is no licence or restrictions associated with it over and above that of the
Dispatch.php module.

## TODO
There are still some items to test out here.

+ Add a few more comments to the Class files to better document their functionality
+ Adding a DELETE /api/chain to allow the Blockchain to be deleted and reset (just for testing!).
+ Updating the HTML pages to allow addition of Transactions to a list and then Commit the list to the Blockchain as a
single Block (as in MultipleTransactionBlockchainExample.php)
+ Add a better persistence layer to allow persistence as flat-files or databases items (although this is not much to do
with the Blockchain itself).
+ Adding multi-node support with broadcasting to peers when Blocks are added
+ Further support API routines to allow testing Blockchain validation, searching, etc...
+ Eventually I would like to add the ability to store Multiple Transactions in a merkle tree and use the hash at the 
root of the tree as the checksum for the block. This forces transactional integrity both within and accross the Blocks
Transactions.


   











                                                                                                            
