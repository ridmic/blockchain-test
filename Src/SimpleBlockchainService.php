<?php

namespace Ridmic;

include "../vendor/autoload.php";

use Ridmic\Classes\Chain;
use Ridmic\Classes\BlockChain;

/**
 * Here we are using the 'Dispatch.php' code from 'noodlehaus'
 */
require 'Utils/Dispatch.php';

// Pull in our own helpers
require 'Utils/Helpers.php';
require 'Routes/APIRoutes.php';
require 'Routes/HTMLRoutes.php';


$blockFile = __DIR__.'/Data/BlockchainSrv.json';


// Create the blockchain
/**
 * @var Chain $chain
 */
$chain = new Chain();
/**
 * @var Blockchain $blockChain
 */
$blockChain = new BlockChain( $chain, 3 );
if ( ! $blockChain->load($blockFile)) {
    $blockChain->initialiseChain();
}

// arguments you pass here get forwarded to the route actions
dispatch($blockChain);


$blockChain->save( $blockFile );
