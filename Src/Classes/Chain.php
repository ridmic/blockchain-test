<?php

namespace Ridmic\Classes;


class Chain implements \Iterator
{
    private     $position   = 0;
    protected   $chain      = [];
    protected   $maxLength  = -1;

    public function __construct( int $maxLength = -1 ) {
        $this->init($maxLength);
    }

    public function init( int $maxLength = -1 ) : bool {
        $this->chain = [];
        $this->maxLength = $maxLength;
        $this->rewind();
        return true;
    }

    public function append(ChainItem $item) : bool {
        if ( $this->maxLength == -1 || $this->length() < $this->maxLength ) {
            $this->chain[] = $item;
            return true;
        }
        return false;
    }

    public function length() : int {
        return count($this->chain);
    }

    public function maxLength() : int {
        return $this->maxLength;
    }

    public function isEmpty() : bool {
        return $this->length() == 0;
    }

    // These allow directed access without affecting any iteration

    public function get( int $index) : ChainItem {
        if ( $index >= 0 && $index < $this->length() ) {
            return $this->chain[$index];
        }
        return new NullChainItem();
    }

    public function first() : ChainItem {
        if ( $this->length() > 0 ) {
            return $this->chain[0];
        }
        return new NullChainItem();
    }

    public function last() : ChainItem {
        if (  $this->length() > 0 ) {
            return $this->chain[$this->length()-1];
        }
        return new NullChainItem();
    }


    // Iterable Interface

    public function rewind()    { $this->position = 0; }
    public function current()   { return $this->chain[$this->position]; }
    public function key()       { return $this->position; }
    public function next()      { ++$this->position; }
    public function valid()     { return $this->position < $this->length() && isset($this->chain[$this->position]); }


    // Magic functions
    public function __toString() : string  {
        $items = [];
        foreach ( $this->chain as $chainItem ) {
            $items[] = (string)$chainItem;
        }
        return sprintf( "{\"chain\":[%s]}", implode(',', $items ) );
    }


}

