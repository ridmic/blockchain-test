<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 07/07/18
 * Time: 12:00
 */

namespace Ridmic\Classes;


class BlockChain
{
    static public $genesisKey = "Genesis-data-block";

    /**
     * @var Chain $chain
     */
    protected $chain;

    /**
     * @var int $difficulty
     */
    protected $difficulty = 2;

    /**
     * @var int $maxDataSize
     */
    protected $maxDataSize = 1024;

    public function __construct( Chain $chain, int $difficulty = 2 , int $maxDataSize = 1024 ) {
        $this->chain = $chain;
        $this->difficulty = $difficulty;
        $this->maxDataSize = $maxDataSize;
    }

    // Accessors
    public function getDifficulty() : int   { return $this->difficulty; }
    public function getMaxDataSize() : int  { return $this->maxDataSize; }
    public function getChain() : Chain      { return $this->chain; }


    public function initialiseChain() {
        $this->chain->init();
        $this->chain->append( new Block( self::$genesisKey, "0" ) );
        return $this->chain->first()->isValid();
    }

    public function mineBlock( string $data ) : ChainItem {

        // Must have some valid data
        if ( is_string($data) && strlen($data) > 0 && strlen($data) <= $this->maxDataSize) {

            // We must have at least our genesis block
            if ( $this->chain->length() > 0 && $this->chain->first()->getContent() === self::$genesisKey ) {

                // Build our candidate block
                $newBlock = new Block( $data , $this->chain->last()->getChecksum());

                // Generate our proof of work
                $proof = $this->proofOfWork( $newBlock );
                if ( $this->addBlock( $newBlock, $proof )) {
                    return $newBlock;
                }
            }
        }
        return new NullChainItem();
    }

    public function proofOfWork( Block $block ) : string {
        $block->setNonce(0);
        $checksum = $block->calculateChecksum();
        $check1 = str_pad("", $this->difficulty, "0", STR_PAD_LEFT);
        $check2 = substr($checksum, 0, $this->difficulty);
        while ( strcmp($check1, $check2) != 0 ) {
            $block->setNonce($block->getNonce()+1);
            $checksum = $block->calculateChecksum();
            $check2 = substr($checksum, 0, $this->difficulty);
        }
        return $checksum;
    }

    public function isValidProof( Block $block, string $proof ) : bool {
        $check1 = str_pad("", $this->difficulty, "0", STR_PAD_LEFT);
        $check2 = substr($proof, 0, $this->difficulty);

        return strcmp($check1, $check2) == 0 && $block->calculateChecksum() == $proof ? true : false;
    }

    public function addBlock( Block $block, string $proof ) : bool {
        $lastBlock = $this->chain->last();

        if ( $lastBlock->getChecksum() === $block->getPreviousChecksum() ) {
           if ( $this->isValidProof( $block, $proof)) {
               $block->setChecksum( $proof );
               return $this->chain->append( $block);
           }
        }
        return false;
    }

    public function validate() : bool {
        // Walk back through the chain ensuring all blocks are valid
        $length = $this->chain->length();
        for ( $index = 0 ; $index < $length ; $index++ ) {
            /**
             * @var Block $currBlock
             */
            $currBlock = $this->chain->get( $length - $index - 1 );

            // Is the block valid?
            if ( ! $currBlock->isValid() ) {
                return false;
            }

            // If we are at the genesis block, then the blockchain is valid?
            if ($currBlock->getContent() === self::$genesisKey ) {
                return true;
            }

            // If our previous checksums don't match, then we are invalid
            /**
             * @var Block $prevBlock
             */
            $prevBlock = $this->chain->get( $length - $index - 2 );
            if ($prevBlock->getChecksum() != $currBlock->getPreviousChecksum() ) {
                return false;
            }
        }
        return false;
    }

    // Just some simple persistence functions for our test

    public function save( string $path ) : bool {
        // This will pump out JSON
        return file_put_contents( $path, $this->chain ) !== false;
    }

    public function load( string $path ) : bool {
        if ( file_exists($path)) {

            // Load the content
            $json = file_get_contents($path);
            $chain = json_decode($json);

            // Initialise our chain and then populate
            $this->chain->init();
            /**
             * @var Block $newBlock
             */
            foreach ( $chain->chain as $block ) {
                $newBlock = new Block( $block->content, $block->previousChecksum );
                $newBlock->setNonce( $block->nonce);
                $newBlock->setChecksum( $block->checksum);
                $newBlock->setTimestamp( $block->timestamp);
                if ( ! $newBlock->isValid() ) {
                    $this->chain->init();
                    return false;
                }
                $this->chain->append( $newBlock );
            }
            return $this->validate();
        }
        return false;
    }
}