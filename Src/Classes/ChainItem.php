<?php

namespace Ridmic\Classes;


abstract class ChainItem
{
    /** @var int */
    protected $timestamp;
    /** @var string */
    protected $checksum;

    public function __construct() {
        // Set the internal state variables
        $this->timestamp = time();
        $this->checksum = $this->calculateChecksum();
    }

    // Accessors
    public function getTimestamp() : int            { return $this->timestamp; }
    public function setTimestamp( int $timestamp )  { $this->timestamp = $timestamp; }
    public function getChecksum() : string          { return $this->checksum; }
    public function setChecksum( string $checksum ) { $this->checksum = $checksum; }

    // Check that our ChainItem is valid
    public function isValid() : bool {
        return $this->getChecksum() == $this->calculateChecksum();
    }

    // Checksum calculation
    abstract public function calculateChecksum() : string ;

    // Magic functions
    public function __toString() : string  {
        return json_encode( [ 'timestamp' => $this->timestamp, 'checksum' => $this->checksum ] );
    }
}
