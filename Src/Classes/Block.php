<?php


namespace Ridmic\Classes;

/**
 * This wraps up a Block (an actual item on the BlockChain)
 *
 * Class Block
 * @package Ridmic\Classes
 */
class Block extends ChainItem
{
    /**
     * @var int
     */
    protected $nonce;
    /**
     * @var string
     */
    protected $previousChecksum;
    /**
     * @var string
     */
    protected $content;

    public function __construct(string $content, string $previousChecksum ) {

        // Set the supplied variables
        $this->nonce = 0;
        $this->content = $content;
        $this->previousChecksum = $previousChecksum;

        // Delegate to our parent for checksum calculation
        parent::__construct();
    }

    // Accessors
    public function getNonce() : int                { return $this->nonce; }
    public function setNonce(int $nonce)            { $this->nonce = $nonce; }
    public function getPreviousChecksum() : string  { return $this->previousChecksum; }
    public function getContent() : string           { return $this->content; }

    // Checksum calculation
    public function calculateChecksum() : string {
        $checksum =  hash("SHA256", sprintf("%d.%d.%s.%s", $this->timestamp,
                                                                        $this->nonce,
                                                                        $this->previousChecksum,
                                                                        $this->content));
        return $checksum;
    }

    // Magic functions
    public function __toString() : string  {
        return json_encode( [ 'nonce' => $this->nonce, 'content' => $this->content, 'previousChecksum' => $this->previousChecksum,
                              'timestamp' => $this->timestamp, 'checksum' => $this->checksum ] );
    }

}