<?php

namespace Ridmic\Classes;

// We create this class here to allow for the 'NullObject' design pattern

class NullChainItem extends ChainItem
{
    static $nullChecksum = "NONE";

    // We always return false here
    public function isValid() : bool { return false; }

    // Checksum calculation
    public function calculateChecksum() : string {
        return self::$nullChecksum;
    }
}
