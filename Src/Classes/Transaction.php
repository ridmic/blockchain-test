<?php

namespace Ridmic\Classes;


/**
 * Class Transaction
 * @package Ridmic\Classes
 *
 * This wraps up a simple transaction object.
 *
 */
class Transaction extends ChainItem
{
    /** @var string */
    protected $owner;
    /** @var string */
    protected $content;

    public function __construct( string $owner, string $content ) {

        // Set the supplied variables
        $this->owner = $owner;
        $this->content = $content;

        // Delegate to our parent for checksum calculation
        parent::__construct();
    }

    // Accessors
    public function getOwner() : string     { return $this->owner; }
    public function getContent() : string   { return $this->content; }

    // Checksum calculation
    public function calculateChecksum() : string {
        $checksum =  hash("SHA256", sprintf("%s.%s.%d", $this->owner, $this->content, $this->timestamp));
        return $checksum;
    }

    // Magic functions
    public function __toString() : string  {
        return json_encode( [ 'owner' => $this->owner, 'content' => $this->content,
                              'timestamp' => $this->timestamp, 'checksum' => $this->checksum ] );
    }

}
