<?php

namespace Ridmic;

use Ridmic\Classes\Block;
use Ridmic\Classes\BlockChain;

// ===============================================================
// This section contains our HTML endpoints
// ===============================================================

// HOME
route('GET', '/', function () {
    $html = phtml(__DIR__.'/../Views/Welcome');
    return response($html);
});

route('GET', '/chain', function ($blockChain) {
    /**
     * @var Blockchain $blockChain
     */
    $html = phtml(__DIR__.'/../Views/Chain', ['chain' => $blockChain->getChain()]);
    return response($html);
});

route('GET', '/chain/:id', function ($args, $blockChain) {
    /**
     * @var Blockchain $blockChain
     * @var Block $chainItem
     */
    $index = intval($args['id']);
    if ( $index >= 0 && $index < $blockChain->getChain()->length() ) {
        $block = $blockChain->getChain()->get($index);
        $html = phtml(__DIR__.'/../Views/Detail', ['block' => $block]);
        return response($html);
    }
    return notFound();
});

route('GET', '.*', function () {
    return notFound();
});