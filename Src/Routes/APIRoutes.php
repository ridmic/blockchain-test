<?php

namespace Ridmic;

use Ridmic\Classes\BlockChain;

// ===============================================================
// This section contains our API endpoints
// ===============================================================

// This route just returns the current blockchain as json
route('GET', '/api/chain', function ($blockChain) {
    /**
     * @var Blockchain $blockChain
     */
    return response($blockChain->getChain(), 200, ['content-type' => 'application/json']);
});

// This route appends a new (mined) block
route('POST', '/api/chain', function ($blockChain) {
    /**
     * @var Blockchain $blockChain
     */
    $content = $_POST['content'] ?? '';
    $interactive = $_POST['interactive'] ?? 'false';

    if (is_string($content) && strlen($content) > 0) {
        if (strlen($content) <= $blockChain->getMaxDataSize()) {
            $block = $blockChain->mineBlock($content);
            if ($block->isValid()) {
                // Success, return the mined block
                if ( $interactive == 'true') {
                    return redirect('/chain');
                }else {
                    return response($block, 200, ['content-type' => 'application/json']);
                }
            } else {
                return error('Unable to mine block', 500);
            }
        } else {
            return error('Content to large', 413);
        }
    }
    return error('Bad or missing content');
});

// This route appends a new (mined) block
route('POST', '/api/chain', function ($blockChain) {
    /**
     * @var Blockchain $blockChain
     */
    $content = $_POST['content'] ?? '';
    $interactive = true;
    if (is_string($content) && strlen($content) > 0) {
        if (strlen($content) <= $blockChain->getMaxDataSize()) {
            $block = $blockChain->mineBlock($content);
            if ($block->isValid()) {
                // Success, return the mined block
                if ( $interactive ) {
                    return redirect('/chain');
                }else {
                    return response($block, 200, ['content-type' => 'application/json']);
                }
            } else {
                return error('Unable to mine block', 500);
            }
        } else {
            return error('Content to large', 413);
        }
    }
    return error('Bad or missing content');
});
