<?php

namespace Ridmic;

include "../vendor/autoload.php";

use Ridmic\Classes\Chain;
use Ridmic\Classes\BlockChain;


/**
 * Create a simple block chain that uses just strings as it's data type
 */
$blockFile = __DIR__.'/Data/Blockchain.json';


// Create the blockchain
/**
 * @var Chain $chain
 */
$chain = new Chain();
/**
 * @var Blockchain $blockChain
 */
$blockChain = new BlockChain( $chain, 3 );
if ( ! $blockChain->load($blockFile)) {
    $blockChain->initialiseChain();
}

// Mine some blocks
$contents = ['aaaaaa', 'bbbbbb', 'cccccc'];
foreach ($contents as $content) {
    printf("Mining block: [%s] ...\n", $content );
    $block = $blockChain->mineBlock( $content );
    if ( ! $block->isValid() ) {
        printf("Unable to mine block: [%s]\n", $content );
    } else {
        printf("Mined block: [%s]\n", $content );
    }
}
printf("The Blockchain is [%s]\n", $blockChain->validate() ? "Valid" : "Invalid" );

$blockChain->save( $blockFile );


//var_dump($blockChain);

