<?php

namespace Ridmic;

include "../vendor/autoload.php";

use Ridmic\Classes\Chain;
use Ridmic\Classes\Transaction;
use Ridmic\Classes\BlockChain;


/**
 * Create a block chain that uses single transactions as it's data type
 */


// Create the blockchain
/**
 * @var Chain $chain
 */
$chain = new Chain();
/**
 * @var Blockchain $blockChain
 */
$blockChain = new BlockChain( $chain, 3 );
$blockChain->initialiseChain();

// Mine some blocks
$contents = ['mike' => 'aaaaaa', 'bob' => 'bbbbbb', 'paul' => 'cccccc'];
foreach ($contents as $owner => $content) {
    /**
     * @var Transaction $content
     */
    $content = new Transaction( $owner, $content );

    printf("Mining block: [%s] ...\n", $content );
    $block = $blockChain->mineBlock( $content );
    if ( ! $block->isValid() ) {
        printf("Unable to mine block: [%s]\n", $content );
    } else {
        printf("Mined block: [%s]\n", $content );
    }
}
printf("The Blockchain is [%s]\n", $blockChain->validate() ? "Valid" : "Invalid" );


//var_dump($blockChain);

