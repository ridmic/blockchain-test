<?php

// ===============================================================
// This section contains some helpers
// ===============================================================
function error( string $message, int $code = 400 ) {
    $json = json_encode( [ 'error'=>$message, 'code'=>$code] );
    return response($json, $code, ['content-type' => 'application/json']);
}

function notFound() {
    $html = phtml(__DIR__.'/../Views/NotFound');
    return response($html);
}
