<?php

namespace Ridmic;

include "../vendor/autoload.php";

use Ridmic\Classes\Chain;
use Ridmic\Classes\Transaction;
use Ridmic\Classes\BlockChain;


/**
 * Create a block chain that uses multiple transactions as it's data type
 */


// Create the blockchain
/**
 * @var Chain $chain
 */
$chain = new Chain();
/**
 * @var Blockchain $blockChain
 */
$blockChain = new BlockChain( $chain, 3 );
$blockChain->initialiseChain();

// Create some transactions
/**
 * @var Chain $chain
 */
$chainTransactions = new Chain();
$chainTransactions->init( 10 );
$contents = ['mike' => 'aaaaaa', 'bob' => 'bbbbbb', 'paul' => 'cccccc'];
foreach ($contents as $owner => $content) {
    printf("Adding transaction [%s] for [%s] ...\n", $content, $owner  );
    /**
     * @var Transaction $trans
     */
    $trans = new Transaction( $owner, $content );
    if ( ! $chainTransactions->append( $trans ) ) {
        printf("Failed to all transaction [%s] for [%s]", $content, $owner );
    }
}

// And mine them as a single block
printf("Mining block ...\n" );
$block = $blockChain->mineBlock( $chainTransactions );
if ( ! $block->isValid() ) {
    printf("Unable to mine block\n" );
} else {
    printf("Mined block\n" );
}
printf("The Blockchain is [%s]\n", $blockChain->validate() ? "Valid" : "Invalid" );


//var_dump($blockChain);

